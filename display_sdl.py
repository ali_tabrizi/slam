import sdl2
import sdl2.ext

class Display(object):
    def __init__(self, W, H):
        # initialize sdl2 window and the display class
        sdl2.ext.init()

        self.W = W
        self.H = H
        
        self.window = sdl2.ext.Window("slam", size=(self.W, self.H))
        self.window.show()

    def show(self, image):
        # event handling such as quiting with escape key
        events = sdl2.ext.get_events()
        for event in events:
            if event.type == sdl2.SDL_QUIT:            
                exit(0)
            if event.type == sdl2.SDL_KEYDOWN:
                if event.key.keysym.sym == sdl2.SDLK_ESCAPE:
                    exit(0)
        # get surface handle of the window and assign the image array to it
        # NOTE: the sdl2 surface has an alpha channel along with RGB channels 
        surf = sdl2.ext.pixels3d(self.window.get_surface())
        surf[:,:,0:3] = image.swapaxes(0,1)
        # refresh the window to show the image 
        self.window.refresh()

