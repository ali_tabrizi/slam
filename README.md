# Simultaneous Localization And Mapping (SLAM)

## Requirements
If you want to display the images and videos using SDL2 library, just install this 
library by following these instructions.

**for UBUNTU**

- first install the sdl2 library by typing this command in terminal
```
sudo apt-get install libsdl2-dev
```
- then install the python wrapper by entering the command
```
    pip install pysdl2  # for python 2
    pip3 install pysdl2 # for python 3 
```
#### How to use sdl2 version
Just extract the sdl_disp folder in the main folder and use slam_sdl.py instead of slam.py


## TODO

