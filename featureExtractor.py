import cv2
import numpy as np


class extractor(object):
    """ 
        Feature Eextractor class
    """

    def __init__(self):
        self.orb = cv2.ORB_create()
        self.bf = cv2.BFMatcher(cv2.NORM_HAMMING)
        self.last_match = None

    def extract(self, img):
        """
            Extracts good features to track and computes corresponding descriptors,
            then matches descriptors of last frame and current frame.

            @param img : input frame as numpy array 

            @returns: a list of matched descriptors 

        """

        # detect some good features to track, features with score below qualityLevel won't be shown
        features = cv2.goodFeaturesToTrack(np.mean(img, axis=2).astype(
            np.uint8), maxCorners=3000, qualityLevel=0.01, minDistance=3)

        # store the extracted feautures in KeyPoint data structure and compute descriptors
        kps = [cv2.KeyPoint(x=f[0][0], y=f[0][1], _size=20) for f in features]
        kps, des = self.orb.compute(img, kps)

        # matching using brute force matching
        good_matches = []
        kps1 = []
        kps2 = []
        if self.last_match is not None:
            matches = self.bf.knnMatch(des, self.last_match['des'], k=2)

            # ratio test to find the matches in same neighborhood
            for m, n in matches:
                if m.distance < 0.75*n.distance:
                    kp1 = kps[m.queryIdx].pt
                    kp2 = self.last_match['kps'][m.trainIdx].pt
                    good_matches.append((kp1, kp2))

        # filter using the fundamental matrix
        # every direction needs to be true in Epipolar geometry
        if len(good_matches) > 0:
            good_matches = np.array(good_matches)
            _, mask = cv2.findFundamentalMat(
                good_matches[:, 0], good_matches[:, 1], cv2.FM_LMEDS)
            good_matches = good_matches[mask.ravel() == 1]

        self.last_match = {'kps': kps, 'des': des}
        return good_matches
