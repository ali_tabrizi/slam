import cv2
import time 
import numpy as np 

W = 1920 // 2
H = 1080 // 2

def process_frame(img):
    img = cv2.resize(img, (W,H))


    return img

if __name__ == "__main__":
    cap = cv2.VideoCapture("inputs/test_countryroad.mp4")

    while cap.isOpened():
        ret, frame = cap.read()
        if ret is True:
            img = process_frame(frame)
            # Display the resulting frame
            cv2.imshow('Frame',img)
        
            # Press Q on keyboard to  exit
            if cv2.waitKey(25) & 0xFF == ord('q'):
                break

        else:
            break

    cap.release()
    cv2.destroyAllWindows()