import cv2
import time
from display_sdl import Display
import numpy as np
from featureExtractor import extractor


# define widht and height for the video to be displayed
W = 1920 * 2 // 5
H = 1080 * 2 // 5

# Initiate video display object
disp = Display(W, H)
featExtrac = extractor()


def process_frame(img):
    """
        a function to process the captured frame from the video feed
    """

    img = cv2.resize(img, (W, H))
    
    # Feature Extraction 
    matches = featExtrac.extract(img)

    if matches is None:
        return
    
    print("{} matches".format(len(matches)))
    for pt1, pt2 in matches:
        u1, v1 = map(lambda x: int(round(x)), pt1)
        u2, v2 = map(lambda x: int(round(x)), pt2)        
        cv2.circle(img, (u1, v1), color=(0, 255, 0), radius=1)
        cv2.line(img, (u1, v1), (u2, v2), color=(255,0,0))
    # for p in kps:
        # u, v = map(lambda x: int(round(x)), p.pt)
        # cv2.circle(img, (u, v), color=(0, 255, 0), radius=1)

    disp.show(img)


# it all starts here
if __name__ == "__main__":
    # capture the video stored in the specified path
    cap = cv2.VideoCapture("inputs/test_countryroad.mp4")

    # read each frame while the capture the path to the video is working
    while cap.isOpened():
        ret, frame = cap.read()
        if ret is True:
            # for each frame do stuff and computations
            process_frame(frame)
        else:
            break
